<?php

/**
 * @file
 * Contains \Drupal\tessitura\Tests\ServiceTesting.
 */

namespace Drupal\tessitura\Tests;

use Drupal\simpletest\WebTestBase;
use Drupal\tessitura\TessituraService;

/**
 * Provides automated tests for the tessitura module.
 */
class ServiceTestingTest extends WebTestBase {

  /**
   * Drupal\tessitura\TessituraService definition.
   *
   * @var Drupal\tessitura\TessituraService
   */
  protected $tessitura_service;
  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => "tessitura ServiceTesting's controller functionality",
      'description' => 'Test Unit for module tessitura and controller ServiceTesting.',
      'group' => 'Other',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests tessitura functionality.
   */
  public function testServiceTesting() {
    // Check that the basic functions of module tessitura.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via App Console.');
  }

}
