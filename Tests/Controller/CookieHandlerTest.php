<?php

/**
 * @file
 * Contains \Drupal\tessitura\Tests\CookieHandler.
 */

namespace Drupal\tessitura\Tests;

use Drupal\simpletest\WebTestBase;
use Drupal\tessitura\Encryption\AESCrypto;

/**
 * Provides automated tests for the tessitura module.
 */
class CookieHandlerTest extends WebTestBase {

  /**
   * Drupal\tessitura\Encryption\AESCrypto definition.
   *
   * @var Drupal\tessitura\Encryption\AESCrypto
   */
  protected $tessitura_encryption;
  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => "tessitura CookieHandler's controller functionality",
      'description' => 'Test Unit for module tessitura and controller CookieHandler.',
      'group' => 'Other',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests tessitura functionality.
   */
  public function testCookieHandler() {
    // Check that the basic functions of module tessitura.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via App Console.');
  }

}
