<?php

/**
 * @file
 * Contains \Drupal\tessitura\Form\EncryptionAdmin.
 */

namespace Drupal\tessitura\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;


const FORM_ID = 'encryption_settings';
const FORM_CONFIG_NAME = 'tessitura.encryption_settings';

/**
 * Class EncryptionAdmin.
 *
 * @package Drupal\tessitura\Form
 */
class EncryptionAdmin extends ConfigFormBase {



  public function __construct()
  {

  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      FORM_CONFIG_NAME
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(FORM_CONFIG_NAME);
    $form['general'] = array(
      '#type' => 'fieldset',
      '#title' => 'General Settings',
    );
    $form['general']['shared_subdomain'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Shared Site Subdomain'),
      '#description' => $this->t('Provide the shared portion of the domain.'),
      '#maxlength' => 128,
      '#size' => 64,
      '#default_value' => $config->get('shared_subdomain'),
    );
    $form['general']['site_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('TNEW Site URL'),
      '#description' => $this->t('URL for the TNEW content'),
      '#maxlength' => 128,
      '#size' => 64,
      '#default_value' => $config->get('site_url'),
    );
    $form['general']['soap_api_uri'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('URL for SOAP API'),
      '#maxlength' => 128,
      '#size' => 64,
      '#default_value' => $config->get('soap_api_uri'),
    );
    $form['general']['local_procedure'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('TR_Local_procedure.ID'),
      '#description' => $this->t('This is the "Sanity Check" precedure id'),
      '#maxlength' => 128,
      '#size' => 64,
      '#default_value' => $config->get('local_procedure'),
    );
    $form['general']['organization_id'] = array(
      '#type' => 'select',
      '#type' => 'textfield',
      '#title' => $this->t('Organiztion ID'),
      '#maxlength' => 128,
      '#size' => 64,
      '#default_value' => $config->get('organization_id'),
    );
    $form['general']['organization_name'] = array(
      '#type' => 'select',
      '#type' => 'textfield',
      '#title' => $this->t('Organiztion Name'),
      '#maxlength' => 128,
      '#size' => 64,
      '#default_value' => $config->get('organization_name'),
    );

    $form['encryption'] = array(
      '#type' => 'fieldset',
      '#title' => 'Encryption Settings',
    );
    $form['encryption']['password'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#description' => $this->t('Please provide AES password hash'),
      '#maxlength' => 128,
      '#size' => 64,
      '#default_value' => $config->get('password'),
    );
    $form['encryption']['hmac_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('HMAC Key'),
      '#description' => $this->t('Please provide HMAC Key'),
      '#maxlength' => 128,
      '#size' => 64,
      '#default_value' => $config->get('hmac_key'),
    );
    $form['encryption']['salt'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('salt'),
      '#description' => $this->t('Please provide salt string'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('salt'),
    );
    $form['encryption']['hash_method'] = array(
      '#type' => 'select',
      '#title' => $this->t('Hash Method'),
      '#description' => $this->t('Please choose the appropriate hasing method'),
      '#options' => array('hash_pbkdf2' => $this->t('hash_pbkdf2'), 'pbkdf2' => $this->t('pbkdf2')),
      '#size' => 1,
      '#default_value' => $config->get('hash_method'),
    );


    return parent::buildForm($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $testValue = trim( $form_state->getValue('shared_subdomain') );
    if( !preg_match('/^[a-z0-9\-\_\.]*/i', $testValue) ) {
        $form_state->setErrorByName('shared_subdomain', 'Invalid URL');
    }
    $testValue = trim( $form_state->getValue('site_url') );
    if(!preg_match('#\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))#iS', $testValue)) {
       $form_state->setErrorByName('site_url', 'Invalid URL');
    }
    $testValue = trim( $form_state->getValue('soap_api_uri') );
    if(!preg_match('#\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))#iS', $testValue)) {
        $form_state->setErrorByName('soap_api_uri', 'Invalid URL');
    }
    $testValue = trim( $form_state->getValue('organization_name') );
    if(!preg_match('/^[a-z0-9]*/i', $testValue)) {
        $form_state->setErrorByName('organization_name', 'Invalid URL');
    }
    $testValue = trim( $form_state->getValue('organization_id') );
    if(!preg_match('/^[0-9]*/i', $testValue)) {
        $form_state->setErrorByName('organization_id', 'Invalid URL');
    }
    $testValue = trim( $form_state->getValue('local_procedure') );
    if(!preg_match('/^[0-9]*/i', $testValue)) {
        $form_state->setErrorByName('local_procedure', 'Invalid URL');
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config(FORM_CONFIG_NAME)
      ->set( 'shared_subdomain', trim( $form_state->getValue('shared_subdomain') ) )
      ->set( 'site_url', trim( $form_state->getValue('site_url') ) )
      ->set( 'soap_api_uri', trim( $form_state->getValue('soap_api_uri') ) )
      ->set( 'local_procedure', trim( $form_state->getValue('local_procedure') ) )
      ->set( 'organization_id', trim( $form_state->getValue('organization_id') ) )
      ->set( 'organization_name', trim( $form_state->getValue('organization_name') ) )
      ->set( 'password', trim( $form_state->getValue('password') ) )
      ->set( 'hmac_key', trim( $form_state->getValue('hmac_key') ) )
      ->set( 'salt', trim( $form_state->getValue('salt') ) )
      ->set( 'hash_method', trim( $form_state->getValue('hash_method') ) )
      ->save();
  }

}
