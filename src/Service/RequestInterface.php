<?php

/**
 * @file
 * Contains \Drupal\tessitura\Service\RequestInterface.
 */

namespace Drupal\tessitura\Service;

/**
 * Interface RequestInterface.
 *
 * @package Drupal\tessitura\Service
 */
interface RequestInterface {

  public function pullData ( $operation , $args );

  public function getResults ();

  public function hasError();

  public function string_error();

}
