<?php

// configuration settings for TessituraService Class to make calls and parse data
// args - are used for pulling data, even empty values must be sent.
// map - used to find data in returns (table), and join elements together to the
// respecive content


define ('MODE_OF_SALE', 68);
define ('SELECT_ALL_OPTIONS', -1); // per doc, -1 for all
define ('BUSINESS_UNIT', 1); // per doc, -1 for all, CORRECTION: PER WAC use 1

return [

  'GetProductions' => [
    'args' => [
      'sStartDate' => '',
      'sEndDate' => '',
      'sPerfType' => '', // unused key, per documentation
      'sArtist' => '',  // unknown
      'iVenueID' => SELECT_ALL_OPTIONS,
      'sKeywords' => '', //unknown
      'iModeOfSale' => MODE_OF_SALE,  // only value we know
      'iBusinessUnit' => BUSINESS_UNIT,
      'sSortString' => '',  // unknown
      ],
    'map' => [
      'Production' => [
        'idKey' => 'prod_season_no',
        'method' => 'primary',
      ],
      'WebContent' => [
        'idKey' => 'orig_inv_no',
        'method' => 'newField',
        'schema' => [
          'key' => 'content_type_desc',
          'value' => 'content_value',
        ],
      ],
      'Keyword' => [
        'idKey' => 'prod_season_no',
        'method' => 'tagging',
        'schema' => [
          'key' => 'keyword',
          'field' => 'keywords'
        ]
      ]
    ],
    ],

  'GetProductionDetailEx2' => [
    'args' => [
      'SessionKey' => '',
      'iPerf_no' => SELECT_ALL_OPTIONS,    // magic value to remove performance id from call
      'iProd_Season_no' => '',
      'iModeOfSale' => MODE_OF_SALE,
      'iBusinessUnit' => BUSINESS_UNIT,
      'sContentTypes' => '',
      'sPerformanceContentTypes' => '',
      ],
    'map' => [
      'Performance' => [
        'idKey' => 'perf_no',
        'method' => 'keyedArray',
      ],
      //'WebContent' => [
      //  'idKey' => 'orig_inv_no',
      //  'method' => 'newFields',
      //  'schema' => [
      //    'key' => 'content_type_desc',
      //    'value' => 'content_value',
      //  ],
      //],
    ],
  ]
];
