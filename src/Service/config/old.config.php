<?php

return [
 'GetPerformances' => array(
    'args' => array(
      'sStartDate' => '',
      'sEndDate' => '',
      'sPerfType' => '',
      'sArtist' => '',
      'iVenueID' => SELECT_ALL_OPTIONS,
      'sKeywords' => '', //unknown
      'iModeOfSale' => MODE_OF_SALE,
      'iBusinessUnit' => BUSINESS_UNIT,
      'sSortString' => '',
      ),
    'map' => array('Performance', 'WebContent'),
   ),
  'GetPerformanceDetail' => array(
    'args' => array(
      'iPerf_no' => 0,
      'iModeOfSale' => MODE_OF_SALE,
      ),
    'parser' => 'App\XMLGroupParser',
    'map' => array(
      'Performance',
      'Price',
      'Credit',
      'PriceType',
      'AllPrice',
      'WebContent',
      ),
    ),
  'GetPerformanceAvailability' => array(
    'args' => array(
      'sStartDate' => '',
      'sEndDate' => '',
      'sPerfType' => '',
      'sArtist' => '',
      'iVenueID' => SELECT_ALL_OPTIONS,
      'sKeywords' => '', //unknown
      'iModeOfSale' => MODE_OF_SALE,
      'iBusinessUnit' => BUSINESS_UNIT,
      'sSortString' => '',  // unknown
      ),
    'map' => array(
      'Production',
      'Table3',
      'Credit',
      'Keyword',
      ),
  ),
  'GetProductionsEx' => array(
    'args' => array(
      'sSessionKey' => '',
      'sStartDate' => '',
      'sEndDate' => '',
      'sPerfType' => '',
      'sArtist' => '',
      'iVenueID' => SELECT_ALL_OPTIONS,
      'sKeywords' => '',
      'iModeOfSale' => MODE_OF_SALE,
      'iBusinessUnit' => BUSINESS_UNIT,
      'sSortString' => '',
      ),
    'parser' => 'App\XMLGroupParser',
    'map' => array(
      'Production',
      'WebContent',
      'Credit',
      'Keyword',
      ),

    ),

  'GetProductionDetail' => array(
    'args' => array(
      'iPerf_no' => '',
      'iModeOfSale' => MODE_OF_SALE,
      'iBusinessUnit' => BUSINESS_UNIT,
      ),
    'parser' => 'App\XMLGroupParser',
    'map' => array(
      'Title',
      'Production',
      'Performance',
      'Credit',
      'WebContent'),
  ),

  'GetSeatsByZone' => array(
    'args' => array(
      'SessionKey' => 'NHKPWQ84TY60RWPP12NH4FAFTFMTE78SER32IT3AXW2DAYAVC6Y5DDQW6DH71814',
      'Zone' => 0,        // docs say that 0 will return all zones
      'PerformanceNumber' => '',
      'PackageNumber' => 0,  // use zero when you use iPerf_no
      ),
    'parser' => 'App\XMLGroupParser',
    'map' => array('Seat'),

  ),
  'GetCountries' => array(
    'args' => array(),
    'parser' => 'App\XMLGroupParser',
    'map' => array('Country'),
  ),
  'GetNewSessionKey' => array(
    'args' => array('sIP' => 'NHKPWQ84TY60RWPP12NH4FAFTFMTE78SER32IT3AXW2DAYAVC6Y5DDQW6DH71814'),
    'parser' => 'App\XMLStringParser',
    'map' => array (),
  ),
  'HelloWorld' => array(
    'args' => array(),
    'parser' => 'App\XMLStringParser',
    'map' => array(),
  )
];
