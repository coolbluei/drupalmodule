<?php

/**
 * @file
 * Contains \Drupal\tessitura\Service\SoapParserInterface.
 */

namespace Drupal\tessitura\Service;

/**
 * Interface SoapParserInterface.
 *
 * @package Drupal\tessitura\Service
 */
interface SoapParserInterface {

  public function parseData($data, $map);

}
