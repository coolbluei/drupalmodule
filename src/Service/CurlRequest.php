<?php

/**
 * @file
 * Contains \Drupal\tessitura\Service\CurlRequest.
 */

namespace Drupal\tessitura\Service;


/**
 * Class CurlRequest.
 *
 * @package Drupal\tessitura\Service
 */
class CurlRequest implements RequestInterface {

  const WOODRUFF_ACCESS_POINT = 'https://prodapi.woodruffcenter.org/tessitura.asmx';

  private $call_results = array();

  /**
   * Constructor.
   */

  public function __construct () {

  }

  public function pullData ($operation = '', $args = array()) {

    $result = array('op' => $operation,
                    'args' => $args);

    if (empty ($operation) ) {
      $result['error'] = array('source' => 'CurlRequest',
                               'origin' => 'operator',
                               'message' => 'Missing Operator');
      return FALSE;
    }

    // build cURL parameters
    $user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11';
    $arguments = '';
    //build url encoded string of arg array
    if (!empty($args)) {
      foreach ($args as $k => $v) {
        $arguments .= urlencode( $k ) . "=" . urlencode ( $v ) . "&";
      }
      if (!empty($arguments)) {
        // if there are some arguments remove the last '&'
        $arguments = substr($arguments, 0, -1);
      }
    }
    $header = array(
      "Accept:text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
      "Accept-Charset:ISO-8859-1,utf-8;q=0.7,*;q=0.3",
      "Accept-Language:en-US,en;q=0.8",
      "Content-Type: application/x-www-form-urlencoded",
      "Content-Length: " . strlen($arguments),
      );

    // start curl construction
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POSTFIELDS , $arguments);
    curl_setopt($ch, CURLOPT_POST      ,1);
    curl_setopt($ch, CURLOPT_URL, self::WOODRUFF_ACCESS_POINT . '/' . $operation);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true); // enable tracking
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);

    curl_setopt($ch, CURLOPT_COOKIESESSION, 1);

    // make curl call
    $response = curl_exec($ch);
    $error = curl_error($ch);
    $result['response'] = $response;

    // store response
    // store these values regardless of results
    $result['headerSent'] = curl_getinfo($ch, CURLINFO_HEADER_OUT );
    $result['http_code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $result['last_url'] = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    $result['payload'] = curl_getinfo($ch, CURLINFO_HEADER_OUT);
    $result ['args'] = $arguments;

    // when no errors are found, store parsed response
    if ($error != "") {
      $result['error'] = array('source' => 'curl',
                               'origin' => $operation,
                               'message' => $error);
    } else {
      $result['error'] = NULL;
      // ?? will there still be a header in case of error?  if so we should retain
      $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
      $result['header'] = substr($response, 0, $header_size);
      $result['body'] = (substr($response, $header_size));
      $result['zipped'] = is_int(strpos($result['header'], 'gzip'));
      if ($result['zipped']) {
        $result['body'] = gzinflate(substr($result['body'], 10, -8));
      }
    }
    curl_close($ch);

    // check http error
    // 500's are problably from tessitura
    if ($result['http_code'] >= 500) {
      // assume its a tessitura error
      $parts = explode (':', $result['body']);
      // take the first part as the context of the problem
      $origin = trim (array_shift($parts));
      // put it back together and save the rest as a message
      // not clear how this is formed, sometimes it is a string and others
      // represent a trace of problems.  Often you can read the offending
      // parameter.  Sometimes not.  UGH.
      $message = trim (implode (':', $parts));
      $result['error'] = array('http_code' => $result['http_code'],
                               'source' => 'tess',
                                'origin' => $origin,
                                'message' => $message);
    } else if ($result['http_code'] >= 400)  {
      // possible permission or bad access point
    }
    $this->call_results = $result;
    return ( is_null($result['error']) );
  }

  public function getResults () {
    return $this->call_results;
  }


  public function hasError() {
    return ( !empty($this->call_results['error']) );
  }

  public function errorToString() {
    if (!empty($this->call_results['error'])) {
      foreach ($this->call_results['error'] as $key => $value) {
        $html [] = "$key = $value";
      }
      return implode ('<br>', $html);
    }
    return '';
  }
}
