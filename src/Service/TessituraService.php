<?php

/**
 * @file
 * Contains \Drupal\tessitura\Service\TessituraService.
 *
 * This service provides methods for pulling data from Tessitura and returning
 * unified arrays.
 *
 * Relies on SoapParser and CurlRequst
 *
 *
 *
 */

namespace Drupal\tessitura\Service;

use Drupal\tessitura\Service\CurlRequest;
use Drupal\tessitura\Service\SoapParser;
use Drupal\tessitura\Service\AESCrypto;
use Psr\Log\LoggerInterface;

/**
 * Class TessituraService.
 *
 * @package Drupal\tessitura\Service
 */
class TessituraService implements TessituraServiceInterface {

  // key in the return data that identifies the specific offering
  const OFFERING_ID_KEY = 'prod_season_no';

  /**
   * Drupal\tessitura\Service\CurlRequest definition.
   *
   * @var Drupal\tessitura\Service\CurlRequest
   */
  protected $curlService;

  /**
   * Drupal\tessitura\Service\SoapParser definition.
   *
   * @var Drupal\tessitura\Service\SoapParser
   */
  protected $parser;

  /**
   * Drupal\tessitura\Logger\WatchLog definition.
   *
   * @var Drupal\tessitura\Logger\WatchLog
   */
  protected $logger;

  /**
   * Drupal\tessitura\Service\AESCrypto definition.
   *
   * @var Drupal\tessitura\Service\AESCrypto
   */
  protected $encryptService;

  private $parsedData;
  private $op;

  /**
   * Constructor.
   */
  public function __construct(CurlRequest $tessitura_curl_request, SoapParser $tessitura_soapparser, LoggerInterface $logger, AESCrypto $tessitura_encryption) {
    $this->curlService = $tessitura_curl_request;
    $this->parser = $tessitura_soapparser;
    $this->logger = $logger;
    $this->encryptService = $tessitura_encryption;
    // config for various services
    $this->services = require ("config/KnownServices.config.php");
    $this->parsedData = array();
    $this->op = '';
  }



  /**
   * Return associative array of offering for the given time span.  If no results
   * found an empty array will be returned.
   *
   * @param $start integer (timestamp)
   * @param $end   integer (timestamp)
   *
   * @return array
   *
   * typical call time for a 12 month range was less than 2 seconds. Record count
   * of 105.  duplicate were observed.
   *
   */

  public function getEventsbyOfferingID ($id = NULL) {
    if (empty($id)) return array();  //No data, no work.
    $args = array (
      'iProd_Season_no' => $id,
    );

    $this->op = 'GetProductionDetailEx2';
    // pull raw data back
    $rawData = $this->callService($this->op, $args);
    //parse to array
    $this->parsedData = $this->parser->parseData ($rawData['body'], $this->services[$this->op]['map']);
    // return just one index for now
    return $this->mungeData();
  }

  /**
   * Pull Offerings available during a date range
   *
   */

  public function getOfferingArrays ($start = '', $end = '' ) {

    $this->op = 'GetProductions';
    $args = array('sStartDate' => $start,
                  'sEndDate' => $end);
    $rawData = $this->callService($this->op, $args);
    if (!empty($rawData)) {
      $this->parsedData = $this->parser->parseData ($rawData['body'], $this->services[$this->op]['map']);
    }
    return $this->mungeData();
  }


  /**
   * Internal method to reduce results to a single record for each id
   *
   */

  private function mungeData () {
    // get map of fields
    $clean = array();
    $mapping = $this->services[$this->op]['map'];
    // review each record
    foreach ($this->parsedData as $id => $record) {
      // review data from each table
      foreach ($record as $table => $results) {
        // get mapping for this table
        if (isset($mapping[$table])) {
          $map = $mapping[$table];
          switch ($map['method']) {
            case 'primary' :
              foreach ($results[0] as $key => $value) {
                $clean[$id][$key] = $value;
              }
              break;
            case 'keyedArray' :
              foreach ($results as $result) {
                $clean[$result[$map['idKey']]] = $result;
              }
              break;
            case 'newField' :
              foreach ($results as $result) {
                if (isset($result[$map['schema']['key']])) {
                  // save data
                  $clean[$result[$map['idKey']]][$result[$map['schema']['key']]] = $result[$map['schema']['value']];
                  // exit foreach, there is only one match
                }
              }
              break;
            case 'tagField' :
              foreach ($results as $result) {
                if ( isset( $result[$map['schema']['key']] ) ) {
                  $clean[$id][$map['schema']['field']][] = $result[$map['schema']['key']];
                }
              }
              break;
          }

        }
      }
      }
      return $clean;
    }
    // start building with keyTable





  /**
   * Non-function stub - Intended to pull details for a single offering
   *
   */

  public function getOffering ($id) {
    return array();
  }

  /**
   * Internal method to pull data from tessitura
   *
   */

  private function callService ($op = '', $data = array()) {
    //get config
    $args = $this->services[$this->op]['args'];
    // merge new data
    foreach ( $args as $key => $orig) {

      if ( $key == 'SessionKey' && empty($data[$key]) ) {
        $data[$key] = '';
      }
      if (isset($data[$key])) {
        $args[$key] = $data[$key];
      }
    }

    $success = $this->curlService->pullData ($this->op, $args);

    if ($success) {
      $this->status = 'pull success';
      return $this->curlService->getResults();
    } else {
      $this->logger->error('Call Failure');
      $this->status = $this->curlService->errorToString();
      return array();
    }

  }


}
