<?php

/**
 * @file
 * Contains \Drupal\tessitura\Service\SoapParser.
 */

namespace Drupal\tessitura\Service;


/**
 * Class SoapParser.
 *
 * @package Drupal\tessitura\Service
 */
class SoapParser implements SoapParserInterface {




  /**
   * Constructor.
   */
  public function __construct() {

  }



  public function parseData ( $rawData = array(),  $maps = array()) {
    //guessing that the xml will start with an opening less than
    // if not present, return data
    if ( '<' !== substr( $rawData, 0, 1 ) ) {
      return array($rawData);
    }
    $sxml = new \SimpleXMLElement ($rawData);

    $mapped = array();
    foreach ( $maps as $table => $tableData ) {
      $idKey = $tableData['idKey'];
      // find elements
      foreach ($sxml->xpath ('//' . $table) as $hit) {

        $data = array();
        foreach ($hit->children() as $ele) {
          // clobber previous hit data within $mapped
          // always force xml results to string
          $data[$ele->getName()] = $ele->__toString();
        }
        if (isset($data[$idKey])) {
          $mapped[$data[$idKey]][$table][]  =  $data;
        } else {
          //missing id
        }
      }
    }
    return $mapped;
  }

}
