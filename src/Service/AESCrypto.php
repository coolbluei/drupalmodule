<?php

/**
 * @file
 * Contains \Drupal\tessitura\Service\AESCrypto.
 */

namespace Drupal\tessitura\Service;
const FORM_CONFIG_NAME = 'tessitura.encryption_settings';
/**
 * Class AESCrypto.
 *
 * @package Drupal\tessitura
 */
class AESCrypto {


    var $password = "2a9KPGKZNmq1VxvXytRgaH2qFGIRR0eQIYMvx6UigsidAqEUCe8GABDByLH6lwNI";
    var $hmacKey = "JAvsySMVn3dC1hN1fBfKAymz6YRC/4rh";

    // Minimum of 8 characters required
    var $salt = "1/L72HuUHZUk0HxRz";
    var $hashMethod = "pdkdf2";

    // These should not change
    var $hmacLength = 32;
    var $iterations = 1000;
    var $keyLength = 32;
    var $blockSize = 16;
    var $algorithm = "sha1";

      /**
   * Constructor.
   */
  public function __construct() {  //__construct($password, $hmacKey, $salt) {
      $config = \Drupal::config(FORM_CONFIG_NAME);
      $this->password = $config->get('password');
      $this->hmacKey = $config->get('hmac_key');
      $this->salt = $config->get('salt');
      $this->hashMethod = $config->get('hash_method');
    }

    function encrypt($plainText)
    {
        $initiationVector = mcrypt_create_iv($this->blockSize);

        $encryptedBytes = $this->encryptInner($initiationVector, $plainText);

        $encryptedMessage = $initiationVector . $encryptedBytes;

        $mac = $this->hashMessage($encryptedMessage);

        $secureMessage = $mac . $encryptedMessage;

        $encryptedText = base64_encode($secureMessage);

        return $encryptedText;
    }

    function decrypt($encryptedText)
    {
        $secureMessage = base64_decode($encryptedText);

        $mac = substr($secureMessage, 0, $this->hmacLength);

        $encryptedMessage = substr($secureMessage, $this->hmacLength);

        $newMac = $this->hashMessage($encryptedMessage);

        if (strcmp($mac, $newMac) !== 0) {
            return "";
        }

        $initiationVector = substr($encryptedMessage,0, $this->blockSize);

        $encryptedBytes = substr($encryptedMessage, $this->blockSize);

        $plainText = $this->decryptInner($initiationVector, $encryptedBytes);

        return $plainText;
    }

    function hashMessage($encryptedMessage)
    {
        return pack("H*", hash_hmac("sha256", $encryptedMessage, $this->hmacKey));
    }

    function encryptInner($initiationVector, $plainText)
    {
        $paddedText = $this->padWithPKCS7($plainText);

        switch ($this->hashMethod) {
          case 'pbkdf2':
            $encryptionKey = $this->pbkdf2($this->algorithm, $this->password, $this->salt, $this->iterations, $this->keyLength, true);
            break;
          default:
            $encryptionKey = hash_pbkdf2($this->algorithm, $this->password, $this->salt, $this->iterations, $this->keyLength, true);
        }

        return mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $encryptionKey, $paddedText, MCRYPT_MODE_CBC, $initiationVector);
    }

    function padWithPKCS7($plainText)
    {
        $paddedResult =  $plainText;

        $block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $padding = $block - (strlen($plainText) % $block);
        $paddedResult .= str_repeat(chr($padding), $padding);

        return $paddedResult;
    }

    function decryptInner($initiationVector, $encryptedBytes)
    {
      switch ($this->hashMethod) {
          case 'pbkdf2':
            $encryptionKey = $this->pbkdf2($this->algorithm, $this->password, $this->salt, $this->iterations, $this->keyLength, true);
            break;
          default:
            $encryptionKey = hash_pbkdf2($this->algorithm, $this->password, $this->salt, $this->iterations, $this->keyLength, true);
      }


        return mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $encryptionKey, $encryptedBytes, MCRYPT_MODE_CBC, $initiationVector);
    }


    /**
     * Added to class to remove global function declarations.
     *
     *
     */

    /*
     * PBKDF2 key derivation function as defined by RSA's PKCS #5: https://www.ietf.org/rfc/rfc2898.txt
     * $algorithm - The hash algorithm to use. Recommended: SHA256
     * $password - The password.
     * $salt - A salt that is unique to the password.
     * $count - Iteration count. Higher is better, but slower. Recommended: At least 1000.
     * $key_length - The length of the derived key in bytes.
     * $raw_output - If true, the key is returned in raw binary format. Hex encoded otherwise.
     * Returns: A $key_length-byte key derived from the password and salt.
     *
     * Test vectors can be found here: https://www.ietf.org/rfc/rfc6070.txt
     *
     * This implementation of PBKDF2 was originally created by https://defuse.ca
     * With improvements by http://www.variations-of-shadow.com
     */
    private function pbkdf2($algorithm, $password, $salt, $count, $key_length, $raw_output = false)
    {
        // check for valid algorithm
        $algorithm = strtolower($algorithm);
        if(!in_array($algorithm, hash_algos(), true))
            return NULL;
        //verify parameters
        if($count <= 0 || $key_length <= 0)
            return NULL;

        $hash_length = strlen(hash($algorithm, "", true));
        $block_count = ceil($key_length / $hash_length);

        $output = "";
        for($i = 1; $i <= $block_count; $i++) {
            // $i encoded as 4 bytes, big endian.
            $last = $salt . pack("N", $i);
            // first iteration
            $last = $xorsum = hash_hmac($algorithm, $last, $password, true);
            // perform the other $count - 1 iterations
            for ($j = 1; $j < $count; $j++) {
                $xorsum ^= ($last = hash_hmac($algorithm, $last, $password, true));
            }
            $output .= $xorsum;
        }

        if($raw_output)
            return substr($output, 0, $key_length);
        else
            return bin2hex(substr($output, 0, $key_length));
    }


}
