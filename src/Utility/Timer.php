<?php

namespace Drupal\tessitura\Utility;

/**
 * calculate elapse time in milliseconds to three decimals
 *
 * usage
 *
 *
 * $timer = new Timer();
 * $elapse = $timer->stop();
 *
 * $elapse = $timer->getElapsed();  //you can recall elapse later
 *
 *
 * timer will run from instantiation until stop method is called
 * the stop method will return the elapse calculation or the value can be
 * retrieved later with the method getElapsed.  timer can be restarted.
 *
 *
 */

class Timer {

	private $start; // array of microtime
	private $stop; // array of microtime
	private $elapsed; // microseconds to three decimals
	private $running;  // boolean of current timing state


	public function __construct() {
		$this->start();
	}

	/**
	 * reset/initiate timing
	 */

	public function start() {
		$this->elapsed = 0;
		$this->stop = array();
		$this->start = explode ( ' ', microtime() );
		$this->running = TRUE;
	}

	/**
	 * stop timing
	 *
	 * return string elapse time in milliseconds
	 */

	public function stop() {
		if ($this->running) {
			$this->stop = explode ( ' ', microtime() );
			$seconds = $this->stop[1] - $this->start[1];
			$fractions = number_format($this->stop[0] - $this->start[0], 6);
			$this->elapsed = number_format(1000 * ($seconds + $fractions), 3);
			$this->running = FALSE;
		}
		return $this->elapsed;
	}

	/**
	 * returns the float value of elapsed time
	 *
	 * @return float
	 */

	public function getElapsed() {
		if ($this->running) {
			$this->stop;
		}
		return $this->elapsed;
	}

  /**
   * Return time since start
   *
   */

  public function getInterval() {
    $now = explode ( ' ', microtime);
    $seconds = $now[1] - $this->start[1];
    $fractions = number_format ($now[0] - $this->start[0], 6);
    return number_format (1000 * ($seconds + $fractions), 3);
  }

}
