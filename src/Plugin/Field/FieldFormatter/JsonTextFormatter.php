<?php

/**
 * @file
 * Contains \Drupal\tessitura\Plugin\Field\FieldFormatter\JsonTextFormatter.
 */

namespace Drupal\tessitura\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\text\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'json_text_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "json_text_formatter",
 *   label = @Translation("Json text formatter"),
 *   field_types = {
 *     "string_long"
 *   }
 * )
 */
class JsonTextFormatter extends FormatterBase {

  const MAX_LENGTH = 64;
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      // Implement default settings.
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return array(
      // Implement settings form.
    ) + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = ['#markup' => $this->viewValue($item),
                           '#attached' => array('library'=> array('tessitura/jsontextfield')),];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    $jsonArray = json_decode($item->value, TRUE);
    return '<div class="abreviated"><div class="short">Array (' . count($jsonArray) . ')</div>'
    . '<div class="long">' . $this->recursivePairs ( $jsonArray ) . '</div></div>';
  }

  private function recursivePairs ($elements = array(), $level = 1) {
    // if empty return empty
    if ( empty($elements) || !is_array($elements) ) {
      $level--;
      return '';
    }
    // foreach array as key value
    $output = array();
    foreach ($elements as $key => $value ) {
      // if value is array, inc $level and call recursivePair and string the return
      if (is_array($value)) {
        $value = '(array of ' . count($value) . ' elements)';
      } else {
        // truncate long values
        if (strlen($value) > self::MAX_LENGTH ) {
          $value = '<div class="abreviated"><div class="short">' . substr($value, 0, self::MAX_LENGTH ) . '... (click to see more)</div>
                    <div class="long">' . $value . '</div></div>';
        }
      }
      //format for this level and return string
      $output [] = "<td>$key</td><td>$value<td>";
    }
    return '<table><tbody><tr>' . implode ( '</tr><tr>', $output ) . '</tr></tbody></table>';
  }

}
