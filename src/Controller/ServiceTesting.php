<?php

/**
 * @file
 * Contains \Drupal\tessitura\Controller\ServiceTesting.
 */

namespace Drupal\tessitura\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\tessitura\Service\TessituraService;

/**
 * Class ServiceTesting.
 *
 * @package Drupal\tessitura\Controller
 */
class ServiceTesting extends ControllerBase {

  const MAX_ITTERATIONS = 40;

  /**
   * Drupal\tessitura\Service\TessituraService definition.
   *
   * @var Drupal\tessitura\Service\TessituraService
   */
  protected $tessitura_service;
  /**
   * {@inheritdoc}
   */
  public function __construct(TessituraService $tessitura_service) {
    $this->tessitura_service = $tessitura_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tessitura.service')
    );
  }



  /**
   * Route Controller - test cron data pull
   *
   */

  public function cron () {
    _tessitura_pull_data_on_cron($this->tessitura_service);

    return [
        '#type' => 'markup',
        '#markup' => '<p>Completed running tessitura_pull_data_on_cron()</p>',
    ];
  }


  /**
   * Route Controller - pull all offerings for the next 12 months
   *
   */

  public function offerings() {
    $timer = new \Drupal\tessitura\Utility\Timer;
    $log = array();

    // get partner mapping for offerings
    // get current list of offerings
    $drupalOfferings = _tessitura_get_current_offerings ();
    // pull offerings for the next 12 months
    $start = date ('c', time());
    $end = date ('c', strtotime('+4month'));

    $offerings = $this->tessitura_service->getOfferingArrays($start, $end);
    // iterate through offerings
    foreach ($offerings as $id => $offering) {
      if (isset($drupalOfferings[$id])) {
        $exists++;
        $current = $drupalOfferings[$id];
          if (TRUE) { //($current['status'] !== 'excluded') {
            $newSHA = sha1(json_encode($offering));
            $oldSHA = sha1($current->json);
            // if JSON is different
            if ($oldSHA !== $newSHA) {
              // update content
              // load current event
              // update JSON
              // dig deeper and see if change is part of content
              if ( _tessitura_entity_is_different('offering', $offering, json_decode ($current->json, TRUE) ) ) {
                // update event with new values
              } else {
                // change in json is not related to current content
                $log['tessChange'][] = $id;
              }
              // save event
              $log['updated'][] = $id;
            } else {// else JSON is the same
              // skip to next event
              $log['matched'][] = $id;
            }
          } else  {  //else status is excluded
            // skip to next event
          }
      } else {
        // get performances from Tess
        $events = $this->tessitura_service->getEventsbyOfferingID($id);
        if (!empty($events)) {
          $event = array_shift($events);
          // add space to $offering
          $offering['field_space'] = _tessitura_get_space ($event);
        }
        //make a new offering
        $node = entity_create( 'node', _tessitura_build_offering($offering) );
        // if node saved, return nid
        $newID = ($node->save() == 1) ? $node->id(): 0;
        $log['new'][] = $newID;
      }

    }
    $log['elapsed'] = $timer->stop();
    $output = '';
    foreach ($log as $lable => $value) {
      if (is_array($value)) {
        $value = 'array ('. $count . ')';
      }
      $output .= "[$lable:$value]";
    }
    return [
        '#type' => 'markup',
        '#markup' => $output,
    ];
  }


}
