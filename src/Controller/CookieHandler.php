<?php

/**
 * @file
 * Contains \Drupal\tessitura\Controller\CookieHandler.
 */

namespace Drupal\tessitura\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\tessitura\Service\AESCrypto;

/**
 * Class CookieHandler.
 *
 * @package Drupal\tessitura\Controller
 */
class CookieHandler extends ControllerBase {

  /**
   * Drupal\tessitura\AESCrypto definition.
   *
   * @var Drupal\tessitura\AESCrypto
   */
  protected $tessitura_encryption;
  /**
   * {@inheritdoc}
   */
  public function __construct(AESCrypto $tessitura_encryption) {
    $this->tessitura_encryption = $tessitura_encryption;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tessitura.encryption')
    );
  }

  /**
   * Route Controller - test handling decryption, and cookie reading
   *
   * @return string
   *   Return Hello string.
   */
  public function decrypt() {
    return [
        '#type' => 'markup',
        '#markup' => '<p>Ajax control method</p>',
    ];
  }

    /**
   *  Route Controller - verify encryption function
   *
   * @return string
   *   Verify encryption function
   */
  public function test() {
    $phrase = 'This is not for human consumption';
    $string2 = 'JdttKdNzzZLko0JnNLzJZ5L0VybuKFQxcP7qV7IW4hHIn0JRS1f94obtr6kX6Xpb3lFPgjvbetgzD2BuPrclosfm0kmw53gbiICjTS60QCUvreyY+37ZO7gSM3uebJ7o';
    $string3 = 'cB+73Z3LWO3pZA5mb7cOuH8wA0a9zSrqaKzVIGcTE/ulRMVqc25NajadNtmMfA5qftx5My5RGPUHSKjCCCogh6r03UKYjvT378SGl0oxioStPneld2ZKRbEwretHiLhA';
    $string4 = 'hfH7PhgU0h+5t/NoqBwR75/pItsAxJbTP1WBmsaI/6A4cb06x76cXerHhJBIfdl81jaiQa0p0RCHkDX6bkVPsiHjAvmn63Np7CYUdN0HpBp8Df+Wd/pcYuATuV5viFiJ';

    $decrypt2 = $this->tessitura_encryption->decrypt($string2);
    $decrypt3 = $this->tessitura_encryption->decrypt($string3);
    $decrypt4 = $this->tessitura_encryption->decrypt($string4);
    $secure = $this->tessitura_encryption->encrypt($phrase);
    $decrypted = $this->tessitura_encryption->decrypt($secure);

    dsm($this->tessitura_encryption);
    $output = "<p>Clean phrase = $phrase</p>
                <p>Secured = $secure</p>
                <p>Decrypted = $decrypted</p>
                <p>Decrypt3 = $decrypt3</p>
                <p>Decrypt4 = $decrypt4</p>
                <p>Decrypt2 = $decrypt2</p>";
    return [
        '#type' => 'markup',
        '#markup' => $output,
    ];
  }

}
